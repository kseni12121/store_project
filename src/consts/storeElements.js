import blackShirt from "../imgs/black_long_shirt.png"
import blueTSHirt from "../imgs/blue_t-shirt.png"
import pinkPants from "../imgs/blush_pink_pants.png"
import brownDress from "../imgs/brown_dress.png"
import creamPants from "../imgs/cream_pants.png"
import grayPants from "../imgs/gray_pants.png"
import greenDress from "../imgs/green_dress.png"
import greenShirt from "../imgs/green_shirt.png"
import whiteDress from "../imgs/white_pants.png"
import whitePants from "../imgs/white_long_dress.png"
import whiteShirt from "../imgs/white_shirt.png"

export const STORE_ELEMENTS = [

    {
        name: "Black Shirt",
        type: "shirt",
        pic: blackShirt,
        size: "Medium",
        color: "black",
        price: "30$"
    },
    {
        name: "Black Shirt",
        type: "shirt",
        pic: blackShirt,
        size: "Small",
        color: "black",
        price: "30$"
    },
    {
        name: "Blue T-Shirt",
        type: "shirt",
        pic: blueTSHirt,
        size: "Extra Large",
        color: "blue",
        price: "18$"
    },
    {
        name: "Green Shirt",
        type: "shirt",
        pic: greenShirt,
        size: "Small",
        color: "green",
        price: "10$"
    },
    {
        name: "Green Shirt",
        type: "shirt",
        pic: greenShirt,
        size: "Extra Large",
        color: "green",
        price: "10$"
    },
    {
        name: "White Shirt" ,
        type: "shirt",
        pic: whiteShirt,
        size: "Large",
        color: "white",
        price: "15$"
    },
    {
        name: "White Shirt" ,
        type: "shirt",
        pic: whiteShirt,
        size: "Medium",
        color: "white",
        price: "15$"
    },
    {
        name: "White Shirt" ,
        type: "shirt",
        pic: whiteShirt,
        size: "Extra Large",
        color: "white",
        price: "15$"
    },
    {
        name: "Blush Pink Pants",
        type: "pants" ,
        pic: pinkPants,
        size: "Small",
        color: "pink",
        price: "20$"
    },
    {
        name: "Blush Pink Pants",
        type: "pants" ,
        pic: pinkPants,
        size: "Large",
        color: "pink",
        price: "20$"
    },
    {
        name: "Cream Pants",
        type: "pants",
        pic: creamPants,
        size: "Large",
        color: "cream",
        price: "20$"
    },
    {
        name: "Cream Pants",
        type: "pants",
        pic: creamPants,
        size: "Small",
        color: "cream",
        price: "20$"
    },
    {
        name: "Cream Pants",
        type: "pants",
        pic: creamPants,
        size: "Extra Small",
        color: "cream",
        price: "20$"
    },
    {
        name: "Gray Pants",
        type: "pants",
        pic: grayPants,
        size: "Medium",
        color: "gray",
        price: "10$"
    },
    {
        name: "Gray Pants",
        type: "pants",
        pic: grayPants,
        size: "Small",
        color: "gray",
        price: "10$"
    },
    {
        name: "White Pants",
        type: "pants",
        pic: whitePants,
        size: "Extra Small",
        color: "white",
        price: "22$"
    },
    {
        name: "White Pants",
        type: "pants",
        pic: whitePants,
        size: "Medium",
        color: "white",
        price: "22$"
    },
    {
        name: "Brown Dress",
        type: "dress",
        pic: brownDress,
        size: "Large",
        color: "brown",
        price: "25$"
    },
    {
        name: "Brown Dress",
        type: "dress",
        pic: brownDress,
        size: "Small",
        color: "brown",
        price: "25$"
    },
    {
        name: "Green Dress",
        type: "dress",
        pic: greenDress,
        size: "Small",
        color: "green",
        price: "20$"
    },
    {
        name: "Green Dress",
        type: "dress",
        pic: greenDress,
        size: "Large",
        color: "green",
        price: "20$"
    },
    {
        name: "White Dress",
        type: "dress",
        pic: whiteDress,
        size: "Medium",
        color: "white",
        price: "30$"
    },
    {
        name: "White Dress",
        type: "dress",
        pic: whiteDress,
        size: "Extra Large",
        color: "white",
        price: "30$"
    },
    {
        name: "White Dress",
        type: "dress",
        pic: whiteDress,
        size: "Extra Small",
        color: "white",
        price: "30$"
    },
]