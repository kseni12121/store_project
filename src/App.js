import MainStore from './Components/MainStore/MainStore';
import './App.css';

function App() {
  return (
    <div className="App">
      <MainStore/>
    </div>
  );
}

export default App;
